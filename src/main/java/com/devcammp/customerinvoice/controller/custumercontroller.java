package com.devcammp.customerinvoice.controller;

import java.util.ArrayList;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcammp.model.Customer;
import com.devcammp.model.Invoice;




@RestController
@RequestMapping("/")
@CrossOrigin
public class custumercontroller {
    @GetMapping("/invoices")
    public ArrayList<Invoice> getInvoices() {
        ArrayList<Invoice> Invoices = new ArrayList<Invoice>();
        Customer customer1 = new Customer(1, "cuong", 20);
        Customer customer2 = new Customer(2, "sang", 32);
        Customer customer3 = new Customer(3, "phuong", 10);

        System.out.println(customer1.toString());
        System.out.println(customer2.toString());
        System.out.println(customer3.toString());

        Invoice invoice1 = new Invoice(1, customer3, 3000);
        Invoice invoice2 = new Invoice(2, customer1, 5000);
        Invoice invoice3 = new Invoice(3, customer2, 6000);

        System.out.println(invoice1.toString());
        System.out.println(invoice2.toString());
        System.out.println(invoice3.toString());

        Invoices.add(invoice1);
        Invoices.add(invoice2);
        Invoices.add(invoice3);

        return Invoices;
    }
}
