package com.devcammp.model;

public class Invoice {
    private int id;
    private Customer customer;
    private double amount;
    public Invoice(int id, Customer customer, double amount) {
        this.id = id;
        this.customer = customer;
        this.amount = amount;
    }
    public int getId() {
        return id;
    }
    public Customer getCustomer() {
        return customer;
    }
    public double getAmount() {
        return amount;
    }
    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
    public void setAmount(double amount) {
        this.amount = amount;
    }

    public int getidCustumer() {
        return this.customer.getId();
    }

    public int getDiscountCustumer() {
        return this.customer.getDiscount();
    }
    public double getAmountafterDiscount() {
        return this.amount*getDiscountCustumer()*0.01;
    }
    @Override
    public String toString() {
        return "Invoice [id=" + id + ", customer=" + customer + ", amount=" + amount + "]";
    }
}
